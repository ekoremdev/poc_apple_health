import 'package:flutter/material.dart';
import 'package:health/health.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:poc_apple_health/util.dart';

class AppleHealthApp extends StatefulWidget {
  const AppleHealthApp({super.key});

  @override
  _AppleHealthAppState createState() => _AppleHealthAppState();
}

class _AppleHealthAppState extends State<AppleHealthApp> {
  List<HealthDataPoint> _healthDataList = [];
  AppState _state = AppState.dataNotFetched;
  int _nofSteps = 0;

  late DateTime currentDate;
  late DateTime startDate;
  late DateTime endDate;

  static const dataTypes = dataTypesIOS;

  // Or both READ and WRITE
  final permissions =
      dataTypes.map((e) => HealthDataAccess.READ_WRITE).toList();

  // create a HealthFactory for use in the app
  HealthFactory health = HealthFactory(useHealthConnectIfAvailable: true);

  ///Requests read permissions from user
  /// Authorize, i.e. get permissions to access relevant health data.
  Future<bool> getPermissionsForHealthAppData() async {
    await Permission.activityRecognition.request();

    return await health.requestAuthorization(
      dataTypesIOS,
      permissions: requiredPermissionsToFetchDataTypesIOS,
    );
  }

  /// Fetch data points from the health plugin and show them in the app.
  Future<void> fetchData(startDate, endDate) async {
    setState(() => _state = AppState.fetchingData);

    // Clear old data points
    _healthDataList.clear();

    try {
      // fetch health data
      List<HealthDataPoint> healthData =
          await health.getHealthDataFromTypes(startDate, endDate, dataTypes);
      // save all the new data points (only the first 100)
      _healthDataList.addAll(
          (healthData.length < 100) ? healthData : healthData.sublist(0, 100));
    } catch (error) {
      print("Exception in getHealthDataFromTypes: $error");
    }

    // filter out duplicates
    _healthDataList = HealthFactory.removeDuplicates(_healthDataList);

    // print the results
    for (var x in _healthDataList) {
      print(x);
    }

    // update the UI to display the results
    setState(() {
      _state = _healthDataList.isEmpty ? AppState.noData : AppState.dataReady;
    });
  }

  /// Fetch steps from the health plugin and show them in the app.
  Future<void> fetchStepData(startDate, endDate) async {
    int? steps;

    bool stepsPermission =
        await health.hasPermissions([HealthDataType.STEPS]) ?? false;
    if (!stepsPermission) {
      stepsPermission =
          await health.requestAuthorization([HealthDataType.STEPS]);
    }

    if (stepsPermission) {
      try {
        steps = await health.getTotalStepsInInterval(startDate, endDate);
      } catch (error) {
        print("Caught exception in getTotalStepsInInterval: $error");
      }

      print('Total number of steps: $steps');

      setState(() {
        _nofSteps = (steps == null) ? 0 : steps;
        _state = (steps == null) ? AppState.noData : AppState.stepsReady;
      });
    } else {
      print("Authorization not granted - error in authorization");
      setState(() => _state = AppState.dataNotFetched);
    }
  }

  /// Revoke access to health data. Note, this only has an effect on Android.
  Future<void> revokeAccess() async {
    try {
      await health.revokePermissions();
    } catch (error) {
      print("Caught exception in revokeAccess: $error");
    }
  }

  Widget _contentFetchingData() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
            padding: const EdgeInsets.all(20),
            child: const CircularProgressIndicator(
              strokeWidth: 10,
            )),
        const Text('Fetching data...')
      ],
    );
  }

  Widget _contentDataReady() {
    return ListView.builder(
        itemCount: _healthDataList.length,
        itemBuilder: (_, index) {
          HealthDataPoint p = _healthDataList[index];
          // if (p.value is AudiogramHealthValue) {
          //   return ListTile(
          //     title: Text("${p.typeString}: ${p.value}"),
          //     trailing: Text(p.unitString),
          //     subtitle: Text('${p.dateFrom} - ${p.dateTo}'),
          //   );
          // }
          // if (p.value is WorkoutHealthValue) {
          //   return ListTile(
          //     title: Text(
          //         "${p.typeString}: ${(p.value as WorkoutHealthValue).totalEnergyBurned} ${(p.value as WorkoutHealthValue).totalEnergyBurnedUnit?.name}"),
          //     trailing: Text(
          //         (p.value as WorkoutHealthValue).workoutActivityType.name),
          //     subtitle: Text(
          //       '${p.dateFrom} - ${p.dateTo}',
          //
          //     ),
          //   );
          // }
          return ListTile(
            title: Text("${p.typeString}: ${p.value}"),
            trailing: Text(p.unitString),
            subtitle: Text('${p.dateFrom} - ${p.dateTo}',style: const TextStyle(
              fontWeight: FontWeight.w700,
            ),),
          );
        });
  }

  Widget _contentNoData() {
    return const Text('No Data to show');
  }

  Widget _contentNotFetched() {
    return const Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Press 'Auth' to get permissions to access health data."),
        Text("Press 'Fetch Dat' to get health data."),
        Text("Press 'Add Data' to add some random health data."),
        Text("Press 'Delete Data' to remove some random health data."),
      ],
    );
  }

  Widget _authorized() {
    return const Text('Authorization granted!');
  }

  Widget _authorizationNotGranted() {
    return const Text('Authorization not given. '
        'For iOS check your permissions in Apple Health.');
  }

  Widget _dataAdded() {
    return const Text('Data points inserted successfully!');
  }

  Widget _dataDeleted() {
    return const Text('Data points deleted successfully!');
  }

  Widget _stepsFetched() {
    return Text('Total number of steps: $_nofSteps');
  }

  Widget _dataNotAdded() {
    return const Text('Failed to add data');
  }

  Widget _dataNotDeleted() {
    return const Text('Failed to delete data');
  }

  Widget _content() {
    if (_state == AppState.dataReady) {
      return _contentDataReady();
    } else if (_state == AppState.noData)
      return _contentNoData();
    else if (_state == AppState.fetchingData)
      return _contentFetchingData();
    else if (_state == AppState.authorized)
      return _authorized();
    else if (_state == AppState.authNotGranted)
      return _authorizationNotGranted();
    else if (_state == AppState.dataAdded)
      return _dataAdded();
    else if (_state == AppState.dataDeleted)
      return _dataDeleted();
    else if (_state == AppState.stepsReady)
      return _stepsFetched();
    else if (_state == AppState.dataNotAdded)
      return _dataNotAdded();
    else if (_state == AppState.dataNotDeleted)
      return _dataNotDeleted();
    else
      return _contentNotFetched();
  }

  @override
  void initState() {
    super.initState();
    startDate = DateTime.now().subtract(
      const Duration(
        days: 1,
      ),
    );

    endDate = currentDate = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Apple Health POC'),
        ),
        body: Column(
          children: [
            Wrap(
              spacing: 10,
              children: [
                ElevatedButton(
                  onPressed: () {
                    getPermissionsForHealthAppData();
                  },
                  style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(
                      Colors.blue,
                    ),
                  ),
                  child: const Text(
                    'Permissions',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    fetchData(startDate, endDate);
                  },
                  style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(
                      Colors.blue,
                    ),
                  ),
                  child: const Text(
                    'Show Steps',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
                ElevatedButton(
                  child: const Text('Select date'),
                  onPressed: () async {
                    final selectedDate = await showDatePicker(
                      context: context,
                      initialDate: currentDate,
                      firstDate: DateTime(currentDate.year,
                          currentDate.month - 3, currentDate.day),
                      lastDate: DateTime(currentDate.year,
                          currentDate.month + 3, currentDate.day),
                    );

                    setState(() {
                      startDate = selectedDate!;
                      endDate = selectedDate.add(
                        const Duration(
                          days: 1,
                        ),
                      );
                    });

                    fetchStepData(startDate, endDate);
                  },
                ),
              ],
            ),
            const Divider(thickness: 3),
            Expanded(
              child: Center(
                child: _content(),
              ),
            )
          ],
        ),
      ),
    );
  }
}
