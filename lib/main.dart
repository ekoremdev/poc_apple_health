import 'dart:async';

import 'package:flutter/material.dart';
import 'package:poc_apple_health/apple_health_app.dart';

Future<void> main() async {
  runApp(
    const MaterialApp(
      home: AppleHealthApp(),
    ),
  );
}
