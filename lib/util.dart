import 'package:health/health.dart';

/// List of data types available on iOS
const List<HealthDataType> dataTypesIOS = [
  HealthDataType.STEPS,
  // HealthDataType.ACTIVE_ENERGY_BURNED,
  // HealthDataType.WEIGHT,
  // HealthDataType.WORKOUT,
  // HealthDataType.HEART_RATE,
];

/// List of data types available on iOS
const requiredPermissionsToFetchDataTypesIOS = [
  HealthDataAccess.READ,
  // HealthDataAccess.READ,
  // HealthDataAccess.READ,
  // HealthDataAccess.READ,
  // HealthDataAccess.READ,
];

enum AppState {
  dataNotFetched('DATA_NOT_FETCHED'),
  fetchingData('FETCHING_DATA'),
  dataReady('DATA_READY'),
  noData('NO_DATA'),
  authorized('AUTHORIZED'),
  authNotGranted('AUTH_NOT_GRANTED'),
  dataAdded('DATA_ADDED'),
  dataDeleted('DATA_DELETED'),
  dataNotAdded('DATA_NOT_ADDED'),
  dataNotDeleted('DATA_NOT_DELETED'),
  stepsReady('STEPS_READY');

  const AppState(this.value);

  final String value;
}
